package com.mobilemo.sample.servlet;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import java.io.PrintWriter;
import javax.servlet.ServletException;

public class UploadHandlerServlet extends HttpServlet {

    private static final long serialVersionUID = -7103506577686647281L;

    // private static final String UPLOADED_TOKEN = "THERE_IS_HOPE";
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
        Logger.getLogger(UploadHandlerServlet.class.getName()).log(Level.INFO, "Performing action in UploadHandlerServlet");
        // String token = req.getParameter("token");
        // if (token.equals(UPLOADED_TOKEN)) {
        // res.setStatus(HttpServletResponse.SC_NON_AUTHORITATIVE_INFORMATION);
        // }

        BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
        Map<String, List<BlobKey>> blobs = blobstoreService.getUploads(req);
        List<BlobKey> keys = blobs.get("photo");
        BlobKey blobKey = keys.get(0);

//		res.sendRedirect("/serve?blob-key=" + blobKey.getKeyString());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
        resp.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = resp.getWriter()) {
            out.println(blobstoreService.createUploadUrl("upload"));
        }
    }

}
