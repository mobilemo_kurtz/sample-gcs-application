package com.mobilemo.kurtz;

import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.blobstore.UploadOptions;

public class AppConfig {

	private static final String BUCKET_NAME = "mobilemo-kurtz.appspot.com";

	public String getUplaodUrl() {
		BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		UploadOptions uploadOptions = null;
		if (BUCKET_NAME == null || BUCKET_NAME.isEmpty()) {
			uploadOptions = UploadOptions.Builder.withDefaults();
		} else {
			uploadOptions = UploadOptions.Builder.withGoogleStorageBucketName(BUCKET_NAME);
		}
		return blobstoreService.createUploadUrl("/upload", uploadOptions);
	}
}
