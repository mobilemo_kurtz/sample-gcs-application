<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false"%>
<%@ page import="com.mobilemo.kurtz.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	AppConfig appConfig = new AppConfig();
%>
<!DOCTYPE html>

<head>
<body>
	Test Upload Page
	<div id="upload-form">
		<form id="fileForm" action="<%=appConfig.getUplaodUrl()%>" method="post" enctype="multipart/form-data">
			<input id="input-file" type="file" name="photo" onchange="onFileSelected()"> 
			<input id="btn-upload" type="submit" value="Upload">

		</form>
	</div>
</body>
</head>